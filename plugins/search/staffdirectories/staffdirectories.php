<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Weblinks Search plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	Search.weblinks
 * @since		1.6
 */
class plgSearchStaffdirectories extends JPlugin
{
	/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.5
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	/**
	 * @return array An array of search areas
	 */
	function onContentSearchAreas() {
		static $areas = array(
			'staffdirectories' => 'Staff Directory'
			);
			return $areas;
	}

	/**
	 * Weblink Search method
	 *
	 * The sql must return the following fields that are used in a common display
	 * routine: href, title, section, created, text, browsernav
	 * @param string Target search string
	 * @param string mathcing option, exact|any|all
	 * @param string ordering option, newest|oldest|popular|alpha|category
	 * @param mixed An array if the search it to be restricted to areas, null if search all
	 */
	function onContentSearch($text, $phrase='', $ordering='', $areas=null)
	{
		$db		= JFactory::getDbo();
		$app	= JFactory::getApplication();
		$user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedViewLevels());


		$searchText = $text;

		if (is_array($areas)) {
			if (!array_intersect($areas, array_keys($this->onContentSearchAreas()))) {
				return array();
			}
		}

		$sContent		= $this->params->get('search_content',		1);
		$sArchived		= $this->params->get('search_archived',		1);
		$limit			= $this->params->def('search_limit',		50);
		$state = array();
		if ($sContent) {
			$state[]=1;
		}
		if ($sArchived) {
			$state[]=2;
		}

		$text = trim($text);
		if ($text == '') {
			return array();
		}
		$section	= JText::_('PLG_SEARCH_STAFFDIRECTORIES');

		$wheres	= array();
		$tag = JFactory::getLanguage()->getTag();
		switch ($phrase)
		{
			
		case 'exact':
				$text		= $db->Quote('%'.$db->escape($text, true).'%', false);
				$wheres2	= array();
				$wheres2[]	= 'a.id LIKE '.$text;
				    $wheres2[]	= 'a.email LIKE '.$text;
					$wheres2[]	= 'a.descp LIKE '.$text;
					$wheres2[]	= 'a.phone LIKE '.$text;
					$wheres2[]	= 'a.name LIKE '.$text;
				$where		= '(' . implode(') OR (', $wheres2) . ')';
				break;

			case 'all':
			case 'any':
			default:
				$words	= explode(' ', $text);
				$wheres = array();
				foreach ($words as $word)
				{
					$word		= $db->Quote('%'.$db->escape($word, true).'%', false);
					$wheres2	= array();
					$wheres2[]	= 'a.id LIKE '.$word;
					    $wheres2[]	= 'a.email LIKE '.$word;
					    $wheres2[]	= 'a.descp LIKE '.$word;
						$wheres2[]	= 'a.phone LIKE '.$word;
						$wheres2[]	= 'a.name LIKE '.$word;
					$wheres[]	= implode(' OR ', $wheres2);
				}
				$where	= '(' . implode(($phrase == 'all' ? ') AND (' : ') OR ('), $wheres) . ')';
				break;
		}

		switch ($ordering)
		{
			case 'oldest':
				$order = 'a.registerDate ASC';
				break;

			case 'alpha':
				$order = 'a.name ASC';
				break;

			case 'newest':
			default:
				$order = 'a.registerDate DESC';
		}
		
		$return = array();
		if (!empty($state)) {
			$query	= $db->getQuery(true);
				$query->select('a.name AS title, CONCAT(a.designation,a.email,a.phone,a.descp) AS text, a.registerDate AS created, a.id as url');
			
			$query->from('#__users AS a');
			$query->where('('.$where.')' );
			$query->order($order);

			$db->setQuery($query, 0, $limit);
			$rows = $db->loadObjectList();

			$return = array();
			if ($rows) {
				foreach($rows as $key => $row) {
					$rows[$key]->href = JRoute::_("index.php?option=com_eposts&view=mainshowstaffdirectories&deptid=".$deptid."&cirid=".$row->url."&Itemid=394");
				}
			}
			
			foreach($rows as $key => $weblink) {
					if (searchHelper::checkNoHTML($weblink, $searchText, array('url', 'text', 'title'))) {
						$return[] = $weblink;
					}
			}
		}
		return $return;
	}
}
