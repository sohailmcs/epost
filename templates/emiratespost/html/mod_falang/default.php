<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_falang
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('stylesheet', 'mod_falang/template.css', array(), true);

?>
<div class="mod-languages<?php echo $moduleclass_sfx ?>">
	<ul class="<?php echo $params->get('inline', 1) ? 'lang-inline' : 'lang-block';?>">
	<?php foreach($list as $language):?>
		<?php if (!$language->active):?>
			<li class="<?php echo !empty($language->active) ? 'lang-active' : '';?>">
			<a href="<?php echo $language->link;?>">
				<?php echo $params->get('full_name', 1) ? $language->title_native : strtoupper($language->sef);?>
			</a>
			</li>
		<?php endif;?>
	<?php endforeach;?>
	</ul>
</div>
