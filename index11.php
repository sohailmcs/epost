<?php
function get_members($group=FALSE,$inclusive=FALSE) {
    // Active Directory server
    $ldap_host = "DC01";
    // Active Directory DN
    $ldap_dn = "$group,DC=acro,DC=com";
    // Domain, for purposes of constructing $user
    $ldap_usr_dom = "@".$ldap_host;
    // Active Directory user
    $user = "administrator@acro.com";
    $password = "Allah00";
    // User attributes we want to keep
    // List of User Object properties:
    // http://www.dotnetactivedirectory.com/Understanding_LDAP_Active_Directory_User_Object_Properties.html
    $keep = array(
        "samaccountname",
        "distinguishedname",
		"givenname",
		"displayname",
		"mailnickname",
		"name",
		"userprincipalname",
		"objectcategory",
		"mail"
    );
    // Connect to AD
    $ldap = ldap_connect($ldap_host) or die("Could not connect to LDAP");
	//ldap_bind($ldap,$user.$ldap_usr_dom,$password) or die("Could not bind to LDAP");
    ldap_bind($ldap,$user,$password) or die("Could not bind to LDAP");
    $query .= "(&(objectClass=user)(objectCategory=person))";
    // Uncomment to output queries onto page for debugging
    // print_r($query);
 
    // Search AD
    $results = ldap_search($ldap,$ldap_dn,$query);
    $entries = ldap_get_entries($ldap, $results);
    // Remove first entry (it's always blank)
    array_shift($entries);
    $output = array(); // Declare the output array
     $i = 0; // Counter
    // Build output array
    foreach($entries as $u) {
        foreach($keep as $x) {
        	// Check for attribute
    		if(isset($u[$x][0])) $attrval = $u[$x][0]; else $attrval = NULL;

        	// Append attribute to output array
        	$output[$i][$x] = $attrval;
        }
        $i++;
    }
    return $output;
}
 
// Example Output
//echo '<pre>'; 
$usersAr= array_merge(get_members("CN=Users"), get_members("OU=Acrologix"));
//print_r($usersAr); // Gets all users in 'Users'

 
/*print_r(get_members(
            array("Test Group","Test Group 2")
        )); // EXCLUSIVE: Gets only members that belong to BOTH 'Test Group' AND 'Test Group 2'
 
print_r(get_members(
            array("Test Group","Test Group 2"),TRUE
        )); // INCLUSIVE: Gets members that belong to EITHER 'Test Group' OR 'Test Group 2'*/
//echo '</pre>';		
?>
Total Users: <?php echo count($usersAr);?>
<table width="100%" border="1">
	<tr>
    	<th>samaccountname</th>
    	<th>distinguishedname</th>
    	<th>givenname</th>
    	<th>displayname</th>
    	<th>mailnickname</th>
    	<th>name</th>
    	<th>userprincipalname</th>
    	<th>objectcategory</th>
    	<th>mail</th>
    </tr>
    <?php foreach($usersAr as $key => $user) : ?>
    	<tr>
        	<td><?php print $user['samaccountname']; ?>&nbsp;</td>
        	<td><?php print $user['distinguishedname']; ?>&nbsp;</td>
        	<td><?php print $user['givenname']; ?>&nbsp;</td>
        	<td><?php print $user['displayname']; ?>&nbsp;</td>
        	<td><?php print $user['mailnickname']; ?>&nbsp;</td>
        	<td><?php print $user['name']; ?>&nbsp;</td>
        	<td><?php print $user['userprincipalname']; ?>&nbsp;</td>
        	<td><?php print $user['objectcategory']; ?>&nbsp;</td>
        	<td><?php print $user['mail']; ?>&nbsp;</td>
        </tr>
    <?php endforeach; ?>
</table>