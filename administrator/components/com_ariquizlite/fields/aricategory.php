<?php
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');

JFormHelper::loadFieldClass('list');

class JFormFieldAriCategory extends JFormFieldList
{
	protected $type = 'AriCategory';

	protected function getOptions()
	{
		$selected = $this->form->getValue($this->name);
		
		if ($selected == '') :
			$selected = JRequest::getCmd('aricategory');
		endif;
		$database = &JFactory::getDBO();
		$database->setQuery("SELECT CategoryId as id,CategoryName as name "
			. "\n FROM #__ariquizcategory "
			. "\n ORDER BY CategoryName ASC"
		);
		$listCategories = $database->loadObjectList();

		$options[]   = JHTML::_('select.option', '0', JText::_('- Select Category -') );
		
		foreach($listCategories as $xCategory) {
			$options[] = JHTML::_('select.option', $xCategory->id, $xCategory->name);
		}
		
		$categoryList = JHTML::_('select.genericlist', $cdicategory, $this->name, 'class="inputbox" ', 'value', 'text', $selected);
	   
	    return $options;
	}
}
