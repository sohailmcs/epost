<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AriAdminViewAriAdmin extends JView
{
	public function display($tpl = null) 
	{
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		
		$this->addToolBar();
		
		parent::display($tpl);
		
		$this->setDocument();
	}

	protected function addToolBar() 
	{
		$isNew = ($this->item->QuizId == 0);
		
		JToolBarHelper::title($isNew ? JText::_('COM_HELLOWORLD_MANAGER_HELLOWORLD_NEW') : JText::_('COM_HELLOWORLD_MANAGER_HELLOWORLD_EDIT'), 'generic.png');
				
		JToolBarHelper::apply('ariadmin.apply', 'JTOOLBAR_APPLY');
		JToolBarHelper::save('ariadmin.save', 'JTOOLBAR_SAVE');
		JToolBarHelper::cancel('ariadmin.cancel', 'JTOOLBAR_CLOSE');
	}

	protected function setDocument() 
	{
		$isNew = ($this->item->QuizId < 1);
		$document = JFactory::getDocument();
		$document->setTitle($isNew ? JText::_('COM_HELLOWORLD_HELLOWORLD_CREATING') : JText::_('COM_HELLOWORLD_HELLOWORLD_EDITING'));
	}
}
