<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

class AriAdminControllerAriAdmins extends JControllerAdmin
{
	public function getModel($name = 'AriAdmin', $prefix = 'AriAdminModel') 
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
}
