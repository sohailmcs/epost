<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AriAdminModelAriAdmins extends JModelList
{
	protected function getListQuery() 
	{
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		
		$query->select('QuizId,QuizName,Status');
		$query->from('#__ariquiz');
		
		return $query;
	}
}
