<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class AriAdminModelAriAdmin extends JModelAdmin
{
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_ariadmin.ariadmin', 'ariadmin', array('control' => 'jform', 'load_data' => $loadData));
		return $form;
	}

	protected function loadFormData()
	{
		$database = &JFactory::getDBO();
		$data = JFactory::getApplication()->getUserState('com_ariadmin.edit.ariadmin.data', array());
		if(empty($data)){
			$data = $this->getItem();
			if (isset($data->QuizId)) :
				$database->SetQuery('SELECT COUNT(*) FROM #__ariquizquizcategory WHERE QuizId = "' . $data->QuizId . '"');
				$category = $database->loadResult();
				if ($data->CategoryId > 0) {
					if (!$category) {
					   $database->setQuery("INSERT INTO #__ariquizquizcategory (QuizId, CategoryId) VALUES ('".$data->QuizId."','".$data->CategoryId."')");
					$database->query();
					} else {
					   $database->setQuery("UPDATE #__ariquizquizcategory SET `CategoryId` = '".$data->CategoryId."' WHERE `QuizId` = '".$data->QuizId."'");
					$database->query();
					}
				}
				$database->SetQuery('SELECT CategoryId FROM #__ariquizquizcategory WHERE QuizId = "' . $data->QuizId . '"');
				$category = $database->loadResult();
				$data->CategoryId = $category;
			endif;
		}
		return $data;
	}
    
	public function getTable($name = '', $prefix = 'AriAdminTable', $options = array())
	{
		return parent::getTable($name, $prefix, $options);
	}
}
