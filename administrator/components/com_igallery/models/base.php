<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.modeladmin');

abstract class igalleryModelBase extends JModelAdmin
{
	function getCategory($id)
	{
		$query = 'SELECT * FROM #__igallery WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		$category = $this->_db->loadObject();
	    return $category;
    }
    
	function getProfile($id)
	{
		$query = 'SELECT * FROM #__igallery_profiles WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		$profile = $this->_db->loadObject();
	    return $profile;
    }		
}