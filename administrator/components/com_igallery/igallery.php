<?php
/**
* @package		Ignite Gallery
* @copyright	Copyright (C) 2011 Matthew Thomson. All rights reserved.
* @license		GNU/GPLv2
*/

defined('_JEXEC') or die;

if(!JFactory::getUser()->authorise('core.manage', 'com_igallery'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

require_once(JPATH_COMPONENT_ADMINISTRATOR.DS.'defines.php');
jimport('joomla.application.component.controller');

$controller	= JController::getInstance('Igallery', array('default_view'=>'categories','base_path' => IG_ADMINISTRATOR_COMPONENT) );
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
?>