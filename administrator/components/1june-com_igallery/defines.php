<?php
defined('_JEXEC') or die('Restricted access');
global $jlistConfig;
$config =& JFactory::getConfig();
$filepath = '';
$storgae_server_path = $config->getValue('config.storgae_server_path');

//define the paths used in php
define('IG_ADMINISTRATOR_COMPONENT', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_igallery');
define( 'IG_IMAGE_PATH', $filepath.DS.'images'.DS.'igallery');
define( 'IG_TEMP_PATH', $filepath.DS.'images'.DS.'igallery'.DS.'temp');
define( 'IG_ORIG_PATH', $filepath.DS.'images'.DS.'igallery'.DS.'original');
define( 'IG_RESIZE_PATH', $filepath.DS.'images'.DS.'igallery'.DS.'resized');
define( 'IG_WATERMARK_PATH', $filepath.DS.'images'.DS.'igallery'.DS.'watermark');
define( 'IG_UPLOAD_PATH', IG_ADMINISTRATOR_COMPONENT.DS.'lib'.DS.'uploaders');
define( 'IG_COMPONENT', JPATH_SITE.DS.'components'.DS.'com_igallery'.DS);

$filepath = $config->getValue( 'config.file_path' );
$image_file_access = $filepath.'\\';
//$image_file_access = '';
//define the paths that will be outputted to the brower
define( 'IG_HOST', JURI::root() );
define( 'IG_IMAGE_HTML_RESIZE', $image_file_access.'images\\igallery\\resized\\');
define( 'IG_IMAGE_HTML_ORIG', $image_file_access.'images\\igallery\\original\\');
define( 'IG_IMAGE_HTML_WATERMARK', $image_file_access.'images\\igallery\\watermark\\');
define( 'IG_IMAGE_ASSET_PATH', $image_file_access.'media\\com_igallery\\images/');

/*
//define the paths used in php
define('IG_ADMINISTRATOR_COMPONENT', JPATH_ADMINISTRATOR.DS.'components'.DS.'com_igallery');
define( 'IG_IMAGE_PATH', JPATH_SITE.DS.'images'.DS.'igallery');
define( 'IG_TEMP_PATH', JPATH_SITE.DS.'images'.DS.'igallery'.DS.'temp');
define( 'IG_ORIG_PATH', JPATH_SITE.DS.'images'.DS.'igallery'.DS.'original');
define( 'IG_RESIZE_PATH', JPATH_SITE.DS.'images'.DS.'igallery'.DS.'resized');
define( 'IG_WATERMARK_PATH', JPATH_SITE.DS.'images'.DS.'igallery'.DS.'watermark');
define( 'IG_UPLOAD_PATH', IG_ADMINISTRATOR_COMPONENT.DS.'lib'.DS.'uploaders');
define( 'IG_COMPONENT', JPATH_SITE.DS.'components'.DS.'com_igallery'.DS);

//define the paths that will be outputted to the brower
define( 'IG_HOST', JURI::root() );
define( 'IG_IMAGE_HTML_RESIZE', JURI::root().'images/igallery/resized/');
define( 'IG_IMAGE_HTML_ORIG', JURI::root().'images/igallery/original/');
define( 'IG_IMAGE_HTML_WATERMARK', JURI::root().'images/igallery/watermark/');
define( 'IG_IMAGE_ASSET_PATH', JURI::root().'media/com_igallery/images/');
*/

//import some classes
jimport('joomla.environment.uri' );
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

require_once(IG_ADMINISTRATOR_COMPONENT.DS.'helpers'.DS.'html.php');
require_once(IG_ADMINISTRATOR_COMPONENT.DS.'helpers'.DS.'file.php');
require_once(IG_ADMINISTRATOR_COMPONENT.DS.'helpers'.DS.'upload.php');
require_once(IG_ADMINISTRATOR_COMPONENT.DS.'helpers'.DS.'general.php');
require_once(IG_COMPONENT.DS.'helpers'.DS.'utility.php');
require_once(IG_ADMINISTRATOR_COMPONENT.DS.'models'.DS.'base.php');

//make the base folders if needed
/*igFileHelper::makeFolder($filepath.DS.'images');
igFileHelper::makeFolder(IG_IMAGE_PATH);
igFileHelper::makeFolder(IG_TEMP_PATH);
igFileHelper::makeFolder(IG_ORIG_PATH);
igFileHelper::makeFolder(IG_RESIZE_PATH);
igFileHelper::makeFolder(IG_WATERMARK_PATH);*/

/*if(!is_dir($filepath.DS.'images')){
	mkdir($filepath.DS.'images');
}*/
$url = $storgae_server_path."/severservice.php";
$post_data['filename'] = '';
$post_data['makenewdir'] = base64_encode('images'); // no slash at the end and bigning
$post_data['file'] = ''; //"@$imgFile";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
$fileresponse = curl_exec($ch);
/*if(!is_dir(IG_IMAGE_PATH)){
	mkdir(IG_IMAGE_PATH);
}*/
$url = $storgae_server_path."/severservice.php";
$post_data['filename'] = '';
$post_data['makenewdir'] = base64_encode(IG_IMAGE_PATH); // no slash at the end and bigning
$post_data['file'] = ''; //"@$imgFile";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
$fileresponse = curl_exec($ch);

/*if(!is_dir(IG_TEMP_PATH)){
	mkdir(IG_TEMP_PATH);
}*/
$url = $storgae_server_path."/severservice.php";
$post_data['filename'] = '';
$post_data['makenewdir'] = base64_encode(IG_TEMP_PATH); // no slash at the end and bigning
$post_data['file'] = ''; //"@$imgFile";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
$fileresponse = curl_exec($ch);

/*if(!is_dir(IG_ORIG_PATH)){
	mkdir(IG_ORIG_PATH);
}*/
$url = $storgae_server_path."/severservice.php";
$post_data['filename'] = '';
$post_data['makenewdir'] = base64_encode(IG_ORIG_PATH); // no slash at the end and bigning
$post_data['file'] = ''; //"@$imgFile";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
$fileresponse = curl_exec($ch);

/*if(!is_dir(IG_RESIZE_PATH)){
	mkdir(IG_RESIZE_PATH);
}*/
$url = $storgae_server_path."/severservice.php";
$post_data['filename'] = '';
$post_data['makenewdir'] = base64_encode(IG_RESIZE_PATH); // no slash at the end and bigning
$post_data['file'] = ''; //"@$imgFile";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
$fileresponse = curl_exec($ch);

/*if(!is_dir(IG_WATERMARK_PATH)){
	mkdir(IG_WATERMARK_PATH);
}*/
$url = $storgae_server_path."/severservice.php";
$post_data['filename'] = '';
$post_data['makenewdir'] = base64_encode(IG_WATERMARK_PATH); // no slash at the end and bigning
$post_data['file'] = ''; //"@$imgFile";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
$fileresponse = curl_exec($ch);
?>