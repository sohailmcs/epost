<?php
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

class JFormFieldIcategorycrnt extends JFormField
{
	protected $type = 'Icategorycrnt';

	protected function getInput()
	{
		$selected = $this->form->getValue('gallery_id');
		$where = "WHERE parent = '1'";
		
		$db =& JFactory::getDBO();
	    $query = 'SELECT id as value, name as text FROM #__igallery '.$where.' ORDER BY name, ordering';
	    $db->setQuery($query);
	    $categories = $db->loadObjectList();
		
	    $selectHTML = JHTML::_("select.genericlist", $categories, $this->name, 'class="inputbox"', 'value', 'text', $selected);
	   
	    return $selectHTML;
	}
}