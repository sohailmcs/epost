<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_search
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
?>
<div class="heading">Search Results</div>
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="adminlist">
<?php
$k = 0;
 foreach($this->results as $result) : 
 ?>
 <tr class="<?php echo "row$k"; ?>">
            <td valign="top">
	<div class="title">
		<?php //echo $this->pagination->limitstart + $result->count.'. ';?>
        <?php $this->pagination->limitstart + $result->count;?>
		<?php if ($result->href) :?>
			<a href="<?php echo JRoute::_($result->href); ?>"<?php if ($result->browsernav == 1) :?> target="_blank"<?php endif;?>>
				<?php echo $this->escape($result->title);?>
			</a>
		<?php else:?>
			<?php echo $this->escape($result->title);?>
		<?php endif; ?>
	</div>
	<div class="body">
		<?php echo $result->text; ?>
	</div>
            <div>&nbsp;</div>
    </td>
    </tr>

<?php 
$k = 1 - $k;
endforeach; 
?>
       <tr class="list-footer">
          <td align="center" colspan="7"><?php echo  $this->pagination->getPagesLinks(); ?></td>
          </tr>
        <tr  class="list-footer">
          <td align="center" colspan="7"><?php echo  $this->pagination->getPagesCounter(); ?></td>
          </tr>
    </table>
