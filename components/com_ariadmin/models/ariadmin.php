<?php
defined('_JEXEC') or die;

//jimport('joomla.application.component.modelitem');

//class AriAdminModelAriAdmin extends JModelItem {

jimport('joomla.application.component.modeladmin');

class AriAdminModelAriAdmin extends JModelAdmin {
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_ariadmin.ariadmin', 'ariadmin', array('control' => 'jform', 'load_data' => $loadData));
		//$form = $this->loadForm('com_ariadmin.ariadmin', 'ariadmin', array('control' => 'jform'));
		return $form;
	}

	protected function loadFormData()
	{
		$data = $this->getItem();
		if(empty($data)){
			$data = $this->getItem();
			if (isset($data->QuizId)) :
				$database->SetQuery('SELECT COUNT(*) FROM #__ariquizquizcategory WHERE QuizId = "' . $data->QuizId . '"');
				$category = $database->loadResult();
				if ($data->CategoryId > 0) {
					if (!$category) {
					   $database->setQuery("INSERT INTO #__ariquizquizcategory (QuizId, CategoryId) VALUES ('".$data->QuizId."','".$data->CategoryId."')");
					$database->query();
					} else {
					   $database->setQuery("UPDATE #__ariquizquizcategory SET `CategoryId` = '".$data->CategoryId."' WHERE `QuizId` = '".$data->QuizId."'");
					$database->query();
					}
				}
				$database->SetQuery('SELECT CategoryId FROM #__ariquizquizcategory WHERE QuizId = "' . $data->QuizId . '"');
				$category = $database->loadResult();
				$data->CategoryId = $category;
			endif;
		}
		return $data;
	}
	
	public function getItem() {
		$qid = JRequest::getCmd('id');
		$query = $this->_db->getQuery(true);
		
		$query->select('QC.CategoryId, QC.CategoryName, Q.QuizName, Q.QuizName_ar, Q.Description, Q.Description_ar, Q.QuizId, Q.Status, Q.TotalTime, Q.PassedScore, Q.QuestionCount, Q.QuestionTime');
		$query->from('#__ariquiz as Q');
		$query->leftJoin('#__ariquizquizcategory as QQC ON Q.QuizId = QQC.QuizId');
		$query->leftJoin('#__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId');
		$query->where('Q.QuizId = ' . (int)$qid);
		
		$this->_db->setQuery($query);

		$data = $this->_db->loadObject();
		return $data;
	}
}
