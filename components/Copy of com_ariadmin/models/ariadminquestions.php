<?php
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class AriAdminModelAriadminQuestions extends JModelList
{
	protected function populateState() 
	{
		$app = JFactory::getApplication();
		
		$aricategory = JRequest::getCmd('id');
		$this->setState('acid', $aricategory);
		
		parent::populateState();
	}
	
	public function getListQuery() 
	{
		$acid = $this->getState('acid');
		$query = $this->_db->getQuery(true);
		/*$query = sprintf('SELECT SQ.QuestionVersionId,SQ.QuestionIndex,SQV.QuestionTime,SQV.QuestionCategoryId' . 
			' FROM #__ariquizquestion SQ INNER JOIN #__ariquizquestionversion SQV' . 
			' 	ON SQ.QuestionVersionId = SQV.QuestionVersionId' . 
			' WHERE SQ.Status = %d AND SQ.QuizId = %d %s' . 
			' ORDER BY %s' . 
			' %s', 
			ARI_QUIZ_QUE_STATUS_ACTIVE,
			$quizId, 
			$catPredicate,
			$orderStr, 
			$this->_getLimit(!empty($questionCount) ? 0 : null, $questionCount));
			*/
		$query->select('SQ.QuestionVersionId,SQ.QuestionIndex,SQV.QuestionTime,SQV.QuestionCategoryId');
		$query->from('#__ariquizquestion as SQ');
		$query->innerJoin('#__ariquizquestionversion SQV ON SQ.QuestionVersionId = SQV.QuestionVersionId');
		$query->leftJoin('#__ariquizcategory as QC ON QC.CategoryId = QQC.CategoryId');
		$query->where('SQ.QuizId ='.(int)$acid);
		
		return $query;
	}
}
