<?php 
/**
* @version		1.0.0
* @package		MijoPolls
* @subpackage	MijoPolls
* @copyright	2009-2011 Mijosoft LLC, www.mijosoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AcePolls www.joomace.net
*
* Based on Apoll Component
* @copyright (C) 2009 - 2011 Hristo Genev All rights reserved
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @link http://www.afactory.org
*/

defined('_JEXEC') or die('Restricted access'); ?>

<form action="index.php?option=com_mijopolls&view=adminpolls&task=view&Itemid=<?php print JRequest::getVar('Itemid'); ?>" method="post" name="adminForm" id="adminForm">
	<a class="brightNew" href="<?php print JRoute::_('index.php?option=com_mijopolls&view=adminpoll&task=adminpolls.edit&Itemid=294'); ?>"><?php print JText::_("New Poll"); ?></a>
    <div class="clear"></div>
	<table width="100%">
		<tr>
			<td class="main_filter">
                    	<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" />
						<?php
							$database = &JFactory::getDBO();
							$database->SetQuery( "SELECT count(*) FROM #__mijopolls_polls");
							$total = $database->loadResult();
							$limit = intval( JRequest::getInt( 'limit', 0 ) );
							$session = JFactory::getSession();
							if(!$limit){
								$limit = ($session->get('limit'))? $session->get('limit'): intval($jlistConfig['files.per.side']);
							}else{
								$session->set('limit', $limit);
							}
							$limitstart   = intval( JRequest::getInt( 'limitstart', 0 ) );
							jimport('joomla.html.pagination'); 
							$pageNav = new JPagination( $total, $limitstart, $limit );
							if ($pageNav->limitstart != $limitstart){
								$session->set('jdlimitstart', $pageNav->limitstart);
								$limitstart = $pageNav->limitstart;
							}
							echo $pageNav->getLimitBox();
						?>
                        
				<!--<button onclick="this.form.submit();"><?php echo JText::_('COM_MIJOPOLLS_GO'); ?></button>
				<button onclick="document.getElementById('search').value='';this.form.getElementById('filter_state').value='';this.form.submit();"><?php echo JText::_('COM_MIJOPOLLS_RESET'); ?></button> -->
			</td>
		</tr>
	</table>
	
	<table class="adminlist">
		<thead>
			<tr>
				<th  class="title">
					<?php echo JText::_('COM_MIJOPOLLS_TITLE'); ?>
				</th>
				<th width="5%" align="center">
					<?php echo JText::_('JSTATUS'); ?>
				</th>
				<th width="10%" align="center">
					<?php echo JText::_('COM_MIJOPOLLS_START_DATE'); ?>
				</th>
				<th width="10%" align="center">
					<?php echo JText::_('COM_MIJOPOLLS_END_DATE'); ?>
				</th>
				<th width="5%" align="center">
					<?php echo JText::_('COM_MIJOPOLLS_VOTES'); ?>
				</th>
				<th width="5%" align="center">
					<?php echo JText::_('COM_MIJOPOLLS_OPTIONS'); ?>
				</th>
				<!--<th width="5%" align="center">
					<?php echo JText::_('COM_MIJOPOLLS_LAG'); ?>
				</th>-->
			</tr>
		</thead>
		<tbody>
		<?php
		$k = 0;
		$n=count($this->items);
		
		for ($i=0; $i < $n; $i++) {
			$row = &$this->items[$i];

			$link = JRoute::_('index.php?option=com_mijopolls&view=adminpoll&task=adminpolls.edit&id='.$row->id);

			$checked 	= JHTML::_('grid.checkedout', $row->id, $i);
			$published 	= JHTML::_('grid.published', $row, $i);
		?>
			<tr class="<?php echo "row$k"; ?>">
				<td>
				<?php if (JTable::isCheckedOut($this->user->get('id'), $row->checked_out)) {
					echo $row->title;
				} else {
					?>
					<a href="<?php echo $link; ?>">
						<?php echo $row->title; ?></a>
					<?php
				}
				?>
				</td>
				<td align="center">
					<?php echo JHtml::_('jgrid.published', $row->published, $i);?>
				</td>			
				<td align="center">
					<?php echo JText::sprintf('%s', JHtml::_('date', $row->publish_up, JText::_('DATE_FORMAT_LC5')));?>
				</td>			
				<td align="center">
					<?php echo JText::sprintf('%s', JHtml::_('date', $row->publish_down, JText::_('DATE_FORMAT_LC5')));?>
				</td>
				<td align="center">
					<a href="index.php?option=com_mijopolls&controller=votes&task=view&total=<?php echo $row->votes; ?>&id=<?php echo $row->id; ?>"><?php echo $row->votes; ?></a>
				</td>
				<td align="center">
					<?php echo $row->options; ?>
				</td>
				<!--<td align="center">
					<?php echo $row->lag/60; ?>
				</td>-->
			</tr>
			<?php
				$k = 1 - $k;
			}
			?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="10">&nbsp;
					
				</td>
			</tr>
		</tfoot>
	</table>

	<input type="hidden" name="option" value="com_mijopolls" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="limitstart" value="<?php print JRequest::getInt('limitstart', 0);?>" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_('form.token'); ?>
</form>