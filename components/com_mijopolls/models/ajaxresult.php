<?php
/**
* @version		1.0.0
* @package		MijoPolls
* @subpackage	MijoPolls
* @copyright	2009-2011 Mijosoft LLC, www.mijosoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
* @license		GNU/GPL based on AcePolls www.joomace.net
*
* Based on Apoll Component
* @copyright (C) 2009 - 2011 Hristo Genev All rights reserved
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
* @link http://www.afactory.org
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

class MijopollsModelAjaxResult extends MijosoftModel {

	var $_query = null;
	var $_data = null;
	var $_total = null;
	var $_voted = null;

	function getVoted() {

		$mainframe 	= JFactory::getApplication();
		$poll_id	= JRequest::getInt('id', 0);
		$option_id	= JRequest::getInt('voteid', 0);
		$poll 		= JTable::getInstance('Poll','Table');
		
		if (!$poll->load($poll_id) || $poll->published != 1) {
			$mainframe->redirect('index.php', JText::_('ALERTNOTAUTH'));
			//JError::raiseWarning(404, JText::_('ALERTNOTAUTH'));
			return;
		}
		
		return $this->_voted = 1;
	}

	function getData() {
		if (empty($this->_data)) {
			$query = $this->_buildQuery();
			$this->_data = $this->_getList($query);
		}
		
		return $this->_data;
	}
	
	function getTotal() {
		if (empty($this->_total)) {
			$query = $this->_buildQuery();
			$this->_total = $this->_getListCount($query);
		}
		
		return $this->_total;
	}
	
	function _buildQuery() {
		if (empty($this->_query)) {
			$db	= JFactory::getDBO();
			$poll_id = JRequest::getVar('id', 0, 'GET', 'int');
			
			$this->_query = "SELECT o.id, o.text, o.text_ar, o.color, COUNT(v.id) AS votes" 
			." FROM #__mijopolls_options AS o "
			." LEFT JOIN #__mijopolls_votes AS v "
			." ON o.id = v.option_id "
			." WHERE o.poll_id = ".(int)$poll_id
			." GROUP BY o.id "
			;
		}
		
		return $this->_query;
	}
}