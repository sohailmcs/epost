<?php
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class UserProfileModelUserProfile extends JModelList
{
	protected function getListQuery() 
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$query = "SELECT u.id, u.name, u.designation, u.phone, u.cell, DATE_FORMAT(u.registerDate,'%d.%m.%Y') as registerDate, DATE_FORMAT(u.lastVisitDate,'%d.%m.%Y') as lastVisitDate,";
		$query .= " (select w.profile_value from #__user_profiles w where w.user_id=u.id and w.profile_key='profile.website') as website,";
		$query .= " (SELECT count( c.id ) FROM #__content c WHERE c.created_by = u.id) AS messagesCount FROM #__users u";
		$query .= " ORDER BY u.name ASC";
		return $query;
	}
}