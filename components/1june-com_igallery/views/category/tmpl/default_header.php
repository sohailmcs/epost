<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php if($this->profile->show_cat_title == 1) : ?>
<h2>
	<?php echo strip_tags($this->category->name); ?>
</h2>
<?php endif; ?>