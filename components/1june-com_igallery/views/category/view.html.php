<?php
defined('_JEXEC') or die( 'Restricted access' );

jimport( 'joomla.application.component.view');

class igViewcategory extends JView
{
	function display($tpl = null)
	{
		//initialise
	    $app = JFactory::getApplication();
	    $this->user =& JFactory::getUser();
	    $this->guest = $this->user->get('guest') ? true : false;
	    $document =& JFactory::getDocument();
	    $this->params =& JComponentHelper::getParams('com_igallery');
	    $model =& $this->getModel();
		
	    //get request vars
	    $this->source = JRequest::getCmd('igsource', 'component');
		$catid = JRequest::getInt('igid', 0);
    	$searchChildren = JRequest::getInt('igchild', 0);
    	$this->showMenu = JRequest::getInt('igshowmenu', 1);
    	$tags = JRequest::getVar('igtags', '');
    	$this->type = JRequest::getCmd('igtype', 'category');
    	$profileId = JRequest::getInt('igpid', 0);
        $limit = JRequest::getInt('iglimit', 0);
        $limit = $limit == 0 ? 1000 : $limit;
        $this->Itemid = JRequest::getInt('Itemid', '');
        
        $uniqueid = JRequest::getCmd('iguniqueid', 0);
        $this->uniqueid = !empty($uniqueid) ? $uniqueid : $catid;
        
		if ($this->source != 'component')
		{
			$overridePath = JPATH_BASE.DS.'templates'.DS.$app->getTemplate().DS.'html'.DS.'com_igallery'.DS.$this->getName();
			$this->_addPath('template', $overridePath);
		}
		
        
        //get model data
        $this->category = & $model->getCategory($catid);
		if($this->category == null)
	    {
	    	JError::raise(2, 500, JText::_('IGALERY_NODATA') );
	        return;
	    }
	    
        $profileId = $profileId == 0 ? $this->category->profile : $profileId;
		$this->profile = $model->getProfile($profileId);
		
		if($this->profile == null)
		{
			JError::raise(2, 500, JText::_('Profile Unpublished') );
			return;
		}
		
		if($this->profile->show_large_image == 0 && $this->profile->show_thumbs == 0)
		{
			JError::raise(2, 500, JText::_('Thumbs or main image display must be enabled in profile settings') );
			return;
		}
		
		//check access
		$this->registerLink = $this->params->get('register_link', 'index.php?option=com_user&amp;task=register');
		if( !in_array($this->profile->access, $this->user->authorisedLevels() ) )
		{
			if($this->profile->access == 2)
			{
				header("location: ".$this->registerLink );
				return;
			}
			else
			{
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}
		
		$this->categoryChildren = $model->getCategoryChildren($catid, $this->profile);
		
		switch($this->type)
    	{
    	    case 'random' : $this->photoList = $model->getRandomList($this->profile, $catid, $tags, $searchChildren, $limit);break;
    	    case 'latest' : $this->photoList = $model->getLatestList($this->profile, $catid, $tags, $searchChildren, $limit);break;
    	    case 'hits'   : $this->photoList = $model->getHitsList($this->profile, $catid, $tags, $searchChildren, $limit);break;
    	    case 'rated'  : $this->photoList = $model->getRatedList($this->profile, $catid, $tags, $searchChildren, $limit);break;
    	    default       : $this->photoList = $model->getCategoryImagesList($this->profile, $catid, $tags, $searchChildren, $limit);
    	}
		
    	if($this->profile->menu_pagination == 1)
    	{
	        $this->menuPagination = $model->getPagination($model->menuTotal, $this->profile->menu_pagination_amount);
    	}
    	if($this->profile->thumb_pagination == 1)
    	{
	        $this->thumbPagination = $model->getPagination($model->thumbTotal,$this->profile->thumb_pagination_amount);
    	}

        //get the image files
		if( !empty($this->photoList) )
		{
		    $this->thumbFiles = array();
    	    $this->mainFiles = array();
    	    $this->lboxFiles = array();
    	    $this->lboxThumbFiles = array();

    		for($i=0; $i<count($this->photoList); $i++)
    		{
    		    $row =& $this->photoList[$i];

    			if(! $this->thumbFiles[$i] = igFileHelper::originalToResized($row->filename, $this->profile->thumb_width,
    		    $this->profile->thumb_height, $this->profile->img_quality, $this->profile->crop_thumbs, $row->rotation, $this->profile->round_thumb, $this->profile->round_fill) )
    		    {
    		        return false;
    		    }

    		    if(! $this->mainFiles[$i] = igFileHelper::originalToResized($row->filename, $this->profile->max_width,
    		    $this->profile->max_height, $this->profile->img_quality, $this->profile->crop_main, $row->rotation, $this->profile->round_large, $this->profile->round_fill, 
    		    $this->profile->watermark, $this->profile->watermark_text, $this->profile->watermark_text_color, $this->profile->watermark_text_size, $this->profile->watermark_filename,
    		    $this->profile->watermark_position, $this->profile->watermark_transparency, 0) )
    		    {
    		        return false;
    		    }

    		    if(! $this->lboxThumbFiles[$i] = igFileHelper::originalToResized($row->filename, $this->profile->lbox_thumb_width,
    		    $this->profile->lbox_thumb_height, $this->profile->img_quality, $this->profile->lbox_crop_thumbs, $row->rotation, $this->profile->round_thumb, $this->profile->round_fill) )
    		    {
    		        return false;
    		    }

    		    if(! $this->lboxFiles[$i] = igFileHelper::originalToResized($row->filename, $this->profile->lbox_max_width,
    		    $this->profile->lbox_max_height, $this->profile->img_quality, $this->profile->crop_lbox, $row->rotation, $this->profile->round_large, $this->profile->round_fill,
    		    $this->profile->watermark, $this->profile->watermark_text, $this->profile->watermark_text_color, $this->profile->watermark_text_size,  $this->profile->watermark_filename,
    		    $this->profile->watermark_position, $this->profile->watermark_transparency, 0) )
    		    {
    		        return false;
    		    }
    		}

    		$this->dimensions = igUtilityHelper::getGalleryDimensions($this->mainFiles, $this->lboxFiles, $this->profile);
    		
    		//add the head js
    		$headJs = igUtilityHelper::makeHeadJs($this->category, $this->profile, $this->photoList, $this->dimensions['galleryWidth'],
	        $this->dimensions['galleryLboxWidth'], $this->mainFiles, $this->lboxFiles, $this->thumbFiles, $this->lboxThumbFiles, 
	        $this->source, $catid, $this->uniqueid);
			
			if($this->profile->share_facebook == 1 || $this->profile->lbox_share_facebook == 1 || $this->profile->allow_comments == 4 || $this->profile->lbox_allow_comments == 4)
			{
				$fb_comments_userid = $this->params->get('fb_comments_userid', '');
				if( !empty($fb_comments_userid) )
				{
					$document->addCustomTag('<meta property="fb:admins" content="'.$fb_comments_userid.'" />');
				}
				
				$fb_comments_appid = $this->params->get('fb_comments_appid', '');
				if( !empty($fb_comments_appid) )
				{
					$document->addCustomTag('<meta property="fb:app_id" content="'.$fb_comments_appid.'" />');
				}
				
				if( (empty($fb_comments_userid) || empty($fb_comments_appid) ) && ($this->profile->allow_comments == 4 || $this->profile->lbox_allow_comments == 4) )
				{
					JError::raise(2, 500, JText::_('Facebook comments are On, Please enter a facebook user id and app into the gallery component options, See support common questions') );
				}
			}
				
			$imageFromUrl = JRequest::getCmd('image', '');
				
			if(!empty($imageFromUrl))
			{
				for($i=0; $i<count($this->photoList); $i++)
				{
					if( JFile::stripExt($this->photoList[$i]->filename) == $imageFromUrl)
					{
						$img = IG_IMAGE_HTML_RESIZE.$this->thumbFiles[$i]['folderName'].'/'.$this->thumbFiles[$i]['fullFileName'];
			
						//$imagePath = IG_IMAGE_HTML_RESIZE.$this->thumbFiles[$i]['folderName'].'/'.$this->thumbFiles[$i]['fullFileName'];
						$imagePath = $img;
						$metaTag = '<meta property="og:image" content="'.$imagePath.'" />';
		
						$document->addCustomTag($metaTag);
						
						if(!empty($this->photoList[$i]->alt_text))
						{
							$document->addCustomTag('<meta property="og:title" content="'.$this->photoList[$i]->alt_text.'" />');
						}
						
						if(!empty($this->photoList[$i]->description))
						{
							$imageDescription = $this->photoList[$i]->description;
							$document->addCustomTag('<meta property="og:description" content="'.JFilterOutput::cleanText($imageDescription).'" />');
						}
						
						break;
					}
				}
			}
			
		}
		
		//component only stuff
		if($this->source == 'component')
	    {
    		igUtilityHelper::writeBreadcrumbs($this->category);
    		$document->setTitle($this->category->name);
    		$description = $this->category->gallery_description;
			if(strlen($description) > 2)
			{
		    	$document->setDescription( substr( JFilterOutput::cleanText($description), 0, 200 )  );
			}
	    }
	    
	    parent::display('main');
	}
}