<?php
	defined('ARI_FRAMEWORK_LOADED') or die('Direct Access to this location is not allowed.');

	$language = &JFactory::getLanguage();
	$database = &JFactory::getDBO();
	$tag = $language->get('tag');
	
	$result = $processPage->getVar('result');
	$resultText = $processPage->getVar('resultText');
	$option = $processPage->getVar('option');
	$ticketId = $processPage->getVar('ticketId');
	$infoMsg = $processPage->getVar('infoMsg');
	$printVisible = $processPage->getVar('printVisible');
	$emailVisible = $processPage->getVar('emailVisible');
	//print '<pre>';
	//print_r($result);
	//print '</pre>';
	
	$catid = JRequest::getCmd('aricategory');
	
	$result['QuizName'] = $result['QuizName'];
	
	if ($tag == 'ar-AA') :
		$result['QuizName'] = $result['QuizName_ar'];
	endif;

	?>
<form method="post" action="" style="margin: 4px 4px 4px 4px;">
	<h3><?php print $result['QuizName']; ?></h3>
	<br/><br/>
    <?php if ($catid == 2) : ?>
    <div class="ari_results">
        <label><?php print JText::_('ARI_USERNAME'); ?>:</label>
        <?php print $result['UserName']; ?>
    </div>
    <div class="ari_results">
        <label><?php print JText::_('ARI_PASSEDSCORE'); ?>:</label>
        <?php print round($result['PassedScore']); ?>
    </div>
    <!--<div class="ari_results">
        <label><?php print JText::_('ARI_MAXSCORE'); ?>:</label>
        <?php print $result['MaxScore']; ?>
    </div> -->
    <div class="ari_results">
        <label><?php print JText::_('ARI_USERSCORE'); ?>:</label>
        <?php print $result['UserScore']; ?>
    </div>
    <div class="ari_results">
        <label><?php print JText::_('ARI_PERSCORE'); ?>:</label>
        <?php print $result['PercentScore']; ?>
    </div>
    <div class="ari_results">
        <label><?php print JText::_('ARI_PASSED'); ?>:</label>
        <?php if ($result['UserScore'] >= $result['PassedScore'])  { echo JText::_('ARI_PASSED_MSG');} else {echo JText::_('ARI_FAILED_MSG');} ?>
    
	</div>
    <!--<div class="ari_results">
        <label><?php print JText::_('ARI_STAETDATE'); ?>:</label>
        <?php print JText::sprintf('%s', JHtml::_('date', $result['StartDate'], JText::_('DATE_FORMAT_LC5'))); ?>
    </div>
    <div class="ari_results">
        <label><?php print JText::_('ARI_ENDDATE'); ?>:</label>
        <?php print JText::sprintf('%s', JHtml::_('date', $result['EndDate'], JText::_('DATE_FORMAT_LC5'))); ?>
    </div> -->
    <div class="ari_results">
        <label><?php print JText::_('ARI_SPENTTIME'); ?>:</label>
        <?php print gmdate("H:i:s", $result['SpentTime']); ?>
    </div>
    <?php else : ?>
    	<?php foreach($result['Questions'] as $key => $value) :?>
        	<h4>Question: <?php print strip_tags($value['Question']); ?></h4>
            <?php
			$database->setQuery("SELECT COUNT(*) FROM #__ariquizstatistics WHERE `QuestionVersionId` = '".$value['QuestionVersionId']."'");
            $totalAns = $database->loadResult();
			
			//print $totalAns;
			
            $xmlData = simplexml_load_string($value['Data']);
				$count = 1;
				if ($value[QuestionTypeId] != 4) :
			?>
					<?php foreach($xmlData as $key => $data) : ?>
                        <?php
                        $ans = $data->attributes();
                        $answers = explode(',', $data[0]);
                        
                        $crrAns = $answers[0];
                        if ($tag == 'ar-AA') :
                            $crrAns = $answers[1];
                        endif;
						$database->setQuery("SELECT COUNT(*) FROM #__ariquizstatistics WHERE `Data` LIKE '%".$ans['id']."%'");
						//echo ("SELECT COUNT(*) FROM #__ariquizstatistics WHERE `Data` LIKE '%".$ans['id']."%'");
						$crrselAns = $database->loadResult();
						$percAns = round(($crrselAns/$totalAns) * 100);
						//print $percAns;
						?>
						<div class="ari_results" style="padding-left:20px; line-height:15px;">
							<label>Choice <?php print $count; ?>: <?php print $crrAns; ?></label><br />
							<div style="height:15px; border:1px solid #CCCCCC; background:#F5F5F5;">
								<div style="background:#CCCCCC; width:<?php print $percAns; ?>%; float:left; border:1px solid #000000; text-align:center; height:13px;">
									<?php if ($percAns >= 90) : ?>
									<span><?php print $percAns . '%'; ?></span>
									<?php endif; ?>
								</div>
								<?php if ($percAns < 90) : ?>
								<span style="padding-left:5px;"><?php print $percAns . '%'; ?></span>
								<?php endif; ?>
								<div class="clear"></div>
							</div>
							<?php //print_r($data); ?>
						</div>
                    <?php $count++; endforeach; ?>
				<?php else : ?>
                    <?php
                    $database->setQuery("SELECT COUNT(*) FROM #__ariquizstatistics WHERE `QuestionVersionId` = '".$value['QuestionVersionId']."'");
                    $textAns = $database->loadResult();
					?>
						<div class="ari_results" style="padding-left:20px; line-height:15px;">
							<label>Total Answers : <?php print $textAns; ?></label><br />
                        </div>
                <?php endif; ?>
            <br />
    	<?php endforeach; ?>
    <?php endif; ?>
	<input type="hidden" name="task" value="quiz_result" />
	<input type="hidden" name="ticketId" value="<?php echo $ticketId; ?>" />
	<input type="hidden" name="option" value="<?php echo $option; ?>" />
</form>