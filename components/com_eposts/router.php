<?php
/**
* @version 1.5
* @package JDownloads
* @copyright (C) 2009 www.eposts.com
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* 
*
*/  

defined( '_JEXEC' ) or die( 'Restricted access' );
// error_reporting(E_ALL ^ (E_NOTICE | E_DEPRECATED | E_STRICT) );
error_reporting(E_ERROR | E_PARSE | E_COMPILE_ERROR);

function epostsBuildRoute(&$query) {
    global $jlistConfig;
    
    jimport('joomla.filter.output');
    $db =& JFactory::getDBO();
    
    $segments    = array();
    
    
    if (isset($query['view'])){
        $segments[] = $query['view'];
        unset($query['view']);
    } else {
        if (isset($query['task'])){
            $segments[] = $query['task'];
            unset($query['task']);
        }        
    }    
               
    if (isset($query['viewcategory'])){
        $segments[] = $query['viewcategory'];
        unset($query['viewcategory']);
    }
    if (isset($query['viewdownload']) || isset($query['view.download'])){
        $segments[] = $query['viewdownload'];
        unset($query['viewdownload']);
    } 
    if (isset($query['summary'])){
        $segments[] = $query['summary'];
        unset($query['summary']);
    }
    if (isset($query['finish'])){
        $segments[] = $query['finish'];
        unset($query['finish']);
    }    
    if (isset($query['search'])){
        $segments[] = $query['search'];
        unset($query['search']);
    }
    if (isset($query['searchresult'])){
        $segments[] = $query['searchresult'];
        unset($query['searchresult']);
    }  
    if (isset($query['upload'])){
        $segments[] = $query['upload'];
        unset($query['upload']);
    }          
    if (isset($query['edit'])){
        $segments[] = $query['edit'];
        unset($query['edit']);
    }
    if (isset($query['save'])){
        $segments[] = $query['save'];
        unset($query['save']);
    }
    if (isset($query['catid'])){
        if ($jlistConfig['use.sef.with.file.titles']){
            $db->setQuery("SELECT cat_title FROM #__eposts_cats WHERE cat_id = ".(int) $query['catid']);
            $segments[] = $query['catid'].':'.JFilterOutput::stringURLSafe($db->loadResult());
        } else {
            $segments[] = $query['catid'];
        }
        unset($query['catid']);
    }
    if (isset($query['cid'])){
        if ($jlistConfig['use.sef.with.file.titles']){
            $db->setQuery("SELECT file_title FROM #__eposts_files WHERE file_id = ".(int) $query['cid']);
            $segments[] = $query['cid'].':'.JFilterOutput::stringURLSafe($db->loadResult());
        } else {
            $segments[] = $query['cid'];
        }
        unset($query['cid']);
    }
    if (isset($query['m'])){
        $segments[] = $query['m'];
        unset($query['m']);
    }
    if (isset($query['list'])){
        $segments[] = $query['list'];
        unset($query['list']);
    }
    if (isset($query['user'])){
        $segments[] = $query['user'];
        unset($query['user']);
    }    
    
        
    return $segments;
}

function epostsParseRoute($segments) {
       $vars = array();

       switch($segments[0])
       {
		        /////////////////////////Development Phase VII/////////////////////
				case 'loadimage':
	                   $vars['view'] = 'loadimage';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
		    	/////////////////////////Development Phase VI/////////////////////
		        case 'mainshowjobs':
	                   $vars['view'] = 'mainshowjobs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistjobs':
	                   $vars['view'] = 'mainlistjobs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainapplyjobs':
	                   $vars['view'] = 'mainapplyjobs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowtasks':
	                   $vars['view'] = 'mainshowtasks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   	   
					   
		   ///////////////////////////////////Depevloment Phase V ////////////////////////////
		        case 'empdownload':
	                   $vars['view'] = 'empdownload';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;		 
		 ///////////////////////////////////Depevloment Phase V ////////////////////////////  
	   	///////////////////////////////////Depevloment Phase IV ////////////////////////////
		        case 'newannouncements':
	                   $vars['view'] = 'newannouncements';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listannouncements':
	                   $vars['view'] = 'listannouncements';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowannouncements':
	                   $vars['view'] = 'mainshowannouncements';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistannouncements':
	                   $vars['view'] = 'mainlistannouncements';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
					   
			   case 'newnews':
	                   $vars['view'] = 'newnews';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listnews':
	                   $vars['view'] = 'listnews';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshownews':
	                   $vars['view'] = 'mainshownews';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistnews':
	                   $vars['view'] = 'mainlistnews';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	
	            case 'newobjectives':
	                   $vars['view'] = 'newobjectives';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listobjectives':
	                   $vars['view'] = 'listobjectives';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
				case 'newvisions':
	                   $vars['view'] = 'newvisions';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listvisions':
	                   $vars['view'] = 'listvisions';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowvisions':
	                   $vars['view'] = 'mainshowvisions';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistvisions':
	                   $vars['view'] = 'mainlistvisions';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
					   
			    case 'newalerts':
	                   $vars['view'] = 'newalerts';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listalerts':
	                   $vars['view'] = 'listalerts';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowalerts':
	                   $vars['view'] = 'mainshowalerts';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistalerts':
	                   $vars['view'] = 'mainlistalerts';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
				case 'newpolicies':
	                   $vars['view'] = 'newpolicies';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listpolicies':
	                   $vars['view'] = 'listpolicies';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowpolicies':
	                   $vars['view'] = 'mainshowpolicies';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistpolicies':
	                   $vars['view'] = 'mainlistpolicies';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break; 
	           
			    case 'newquotes':
	                   $vars['view'] = 'newquotes';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listquotes':
	                   $vars['view'] = 'listquotes';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowquotes':
	                   $vars['view'] = 'mainshowquotes';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistquotes':
	                   $vars['view'] = 'mainlistquotes';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break; 
					   
	            case 'newmanuals':
	                   $vars['view'] = 'newmanuals';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'listmanuals':
	                   $vars['view'] = 'listmanuals';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowmanuals':
	                   $vars['view'] = 'mainshowmanuals';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistmanuals':
	                   $vars['view'] = 'mainlistmanuals';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	
					   
				case 'newfaqs':
	                   $vars['view'] = 'newfaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	             case 'listfaqs':
	                   $vars['view'] = 'listfaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowfaqs':
	                   $vars['view'] = 'mainshowfaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistfaqs':
	                   $vars['view'] = 'mainlistfaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	      	
					   
				case 'newbenchmarks':
	                   $vars['view'] = 'newbenchmarks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	             case 'listbenchmarks':
	                   $vars['view'] = 'listbenchmarks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				case 'mainshowbenchmarks':
	                   $vars['view'] = 'mainshowbenchmarks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	            case 'mainlistbenchmarks':
	                   $vars['view'] = 'mainlistbenchmarks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
	   
	             case 'newlinks':
	                   $vars['view'] = 'newlinks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
	             case 'listlinks':
	                   $vars['view'] = 'listlinks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
   //////////////////////////////END Depevloment Phase IV////////////////////////////////		   

		       case 'showsllider':
                       $vars['view'] = 'showsllider';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
		       case 'mainaddempinfo':
                       $vars['view'] = 'mainaddempinfo';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainshowempinfo':
                       $vars['view'] = 'mainshowempinfo';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistlinks':
                       $vars['view'] = 'mainlistlinks';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainshoworgcharts':
                       $vars['view'] = 'mainshoworgcharts';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistorgcharts':
                       $vars['view'] = 'mainlistorgcharts';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'mainshowstaffdirectories':
                       $vars['view'] = 'mainshowstaffdirectories';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainliststaffdirectories':
                       $vars['view'] = 'mainliststaffdirectories';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
		       case 'mainshownewhires':
                       $vars['view'] = 'mainshownewhires';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistnewhires':
                       $vars['view'] = 'mainlistnewhires';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'mainshowdocuments':
                       $vars['view'] = 'mainshowdocuments';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistdocuments':
                       $vars['view'] = 'mainlistdocuments';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'mainshowevents':
                       $vars['view'] = 'mainshowevents';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistevents':
                       $vars['view'] = 'mainlistevents';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainshowevents':
                       $vars['view'] = 'mainshowevents';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistevents':
                       $vars['view'] = 'mainlistevents';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'mainshowtasawaqs':
                       $vars['view'] = 'mainshowtasawaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlisttasawaqs':
                       $vars['view'] = 'mainlisttasawaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainshowcirculars':
                       $vars['view'] = 'mainshowcirculars';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'mainlistcirculars':
                       $vars['view'] = 'mainlistcirculars';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   		   
			   ////////////newpages
			   case 'complaints':
                       $vars['view'] = 'complaints';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'suggestions':
                       $vars['view'] = 'suggestions';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'grievances':
                       $vars['view'] = 'grievances';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;		   
			   
			   case 'newdocument':
                       $vars['view'] = 'newdocument';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'listdocuments':
                       $vars['view'] = 'listdocuments';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
				
			   case 'newevent':
                       $vars['view'] = 'newevent';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'listevents':
                       $vars['view'] = 'listevents';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   
			   case 'newtasawaq':
                       $vars['view'] = 'newtasawaq';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'listtasawaqs':
                       $vars['view'] = 'listtasawaqs';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	   
					   
			   
			   case 'newhires':
                       $vars['view'] = 'newhires';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'listnewhires':
                       $vars['view'] = 'listnewhires';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
					   
					   
			   case 'newmedia':
                       $vars['view'] = 'newmedia';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'media':
                       $vars['view'] = 'media';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   		   
			   case 'listmedia':
                       $vars['view'] = 'listmedia';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;	
					   
			   case 'mainlistmedias':
                       $vars['view'] = 'mainlistmedias';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;		   	   
					   
			   case 'newcircular':
                       $vars['view'] = 'newcircular';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'listcirculars':
                       $vars['view'] = 'listcirculars';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
					   
			   case 'myhomepage':
                       $vars['view'] = 'myhomepage';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;
			   case 'mainmyhomepage':
                       $vars['view'] = 'mainmyhomepage';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
	                   break;		   
					   
				case 'mydepartment':
                       $vars['view'] = 'mydepartment';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;
					   
				case 'listdepartment':
                       $vars['view'] = 'listdepartment';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;
				case 'approvebookingrequest':
                       $vars['view'] = 'approvebookingrequest';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;
				case 'viewbookingrequest':
                       $vars['view'] = 'viewbookingrequest';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;	   	   	   	   
				case 'bookingrequest':
                       $vars['view'] = 'bookingrequest';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;
			    case 'ajaxbookingrequest':
                       $vars['view'] = 'ajaxbookingrequest';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;
			   case 'addUserBookingRequest':
				       $vars['view'] = 'addUserBookingRequest';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;		   	    		   
			   case 'calendar':
                       $vars['view'] = 'calendar';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;
				case 'listresources':
                       $vars['view'] = 'listresources';
                       $catid = explode( ':', $segments[1] );
                       $vars['catid'] = (int) $catid[0];
                       break;	   
                                            
                       
              default: $vars['view'] = 'listresources';
                       break;
      
       }
       return $vars;
} 
?>