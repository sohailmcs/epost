<?php
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.modeladmin');

class igModelimage extends JModelAdmin
{
	
	public function getTable($type = 'igallery_img', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function getForm($data = array(), $loadData = true)
	{
		return;
	}
	
	function getPhoto($id)
	{
		$query = 'SELECT * FROM #__igallery_img WHERE id = '. (int)$id;
		$this->_db->setQuery($query);
		$photo = $this->_db->loadObject();
		return $photo;
	}
	
	function getCategory($id)
	{
		$query = 'SELECT * FROM #__igallery WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		$category = $this->_db->loadObject();
	    return $category;
    }
    
	function getProfile($id)
	{
		$query = 'SELECT * FROM #__igallery_profiles WHERE id = '.(int)$id;
		$this->_db->setQuery($query);
		$profile = $this->_db->loadObject();
	    return $profile;
    }
	
	function addHit($id)
	{
		$row =& $this->getTable('igallery_img');
		$row->load( (int)$id );
		$row->hits = $row->hits + 1;
		if(!$row->store())
		{
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		return true;
	}
	
	function reportImage()
	{
		$id = JRequest::getInt('id', 0);
		$text = JRequest::getVar('report_textarea', '');
		$user =& JFactory::getUser();
		$db	=& JFactory::getDBO();
		
		$siteConfig =& JFactory::getConfig();
		$from = $siteConfig->getValue( 'config.mailfrom' );
		$fromname = $siteConfig->getValue( 'config.fromname' );
		
		$igalleryConfig =& JComponentHelper::getParams('com_igallery');
		$recipient = explode(',', $igalleryConfig->get('notify_emails', '') );
		
		$query = 'SELECT '.
		'#__igallery_img.filename, #__igallery_img.ordering, '.
		'#__igallery.name, #__igallery.id '.
		'FROM #__igallery_img '.
		'INNER JOIN #__igallery ON #__igallery.id = #__igallery_img.gallery_id '.
		'WHERE #__igallery_img.id = '. (int)$id;
		$db->setQuery($query);
		$photo = $db->loadObject();
		
		$subject = $siteConfig->getValue('config.sitename').' : '.JText::_('IMAGE_REPORTED');
		
		if( $user->get('guest') )
		{
			$user->name = JText::_('PUBLIC');
		}
		
		$body =
		JText::_('JGLOBAL_USERNAME').': '.$user->name." \n\n ".
		JText::_('JCATEGORY').': '.$photo->name." \n\n ".
		IG_HOST.'index.php?option=com_igallery&view=category&igid='.$photo->id.'&image='.$photo->ordering.'&Itemid='.JRequest::getInt('Itemid')." \n\n ".
		$text;
		
		
		for($i=0; $i<count($recipient); $i++)
		{
			JUtility::sendMail($from, $fromname, $recipient[$i], $subject, $body);
			
			if($i > 5)
			{
				break;
			}
		}
		return true;
	}
		
}