﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Exchange.WebServices.Data;
using System.Net;
using System.Security.Principal;
using System.Threading;

public partial class AllTasks : System.Web.UI.Page
{
    #region Properties
    DAL objDAL = new DAL();
    ErrorLogs objErrorLogs = new ErrorLogs();
    private DataTable TasksData
    {
        get
        {
            return ViewState["TasksData"] as DataTable;
        }

        set
        {
            ViewState["TasksData"] = value;
        }
    }

    private string SortOrder
    {
        get
        {
            return ViewState["SortOrder"].ToString();
        }

        set
        {
            ViewState["SortOrder"] = value;
        }
    }

    private string SortExpression
    {
        get
        {
            return ViewState["SortExpression"].ToString();
        }

        set
        {
            ViewState["SortExpression"] = value;
        }
    }
    #endregion

    #region Event Handlers
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                //Check either userid and password exists in the database.
				WindowsPrincipal userID = Thread.CurrentPrincipal as WindowsPrincipal; //MOIZ
                string Password = RedirecttoLogin(true, "", "");
                if (Password.Trim() == "")
                    return;
                //string UserName = WindowsIdentity.GetCurrent().Name.Split('\\')[1], DomainName = WindowsIdentity.GetCurrent().Name.Split('\\')[0];  OLD
                  string UserName = userID.Identity.Name.ToString().Split('\\')[1], DomainName = WindowsIdentity.GetCurrent().Name.Split('\\')[0];  //MOIZ
                 // Response.Write("<br>identity : " + UserName);


				SortOrder = "ASC";
                SortExpression = "DueDate";
                DataTable tasksData = GetTasksData(UserName, Password, DomainName);
                TasksData = tasksData;
                gvTasks.DataSource = tasksData;
                gvTasks.DataBind();
            }
        }
        catch (Exception exception)
        {
            objErrorLogs.LogError(exception);
            ShowError(exception.Message);
            RedirecttoLogin(false, "TLAErr", "E");
        }
    }

    private string RedirecttoLogin(bool CheckDB, string Error, string IssueType)
    {
       // string userID = WindowsIdentity.GetCurrent().Name.Split('\\')[1];
		  WindowsPrincipal userID = Thread.CurrentPrincipal as WindowsPrincipal; //MOIZ
		  //Response.Write("<br>identity : " + userID.Identity.Name.ToString().Split('\\')[1]);
		
        if (CheckDB)
        {
            string Password = objDAL.IsUserNameExistinDB("SELECT UserName, CONVERT(Password USING latin1) FROM loginifo Where UserName='" + userID.Identity.Name.ToString().Split('\\')[1] + "';");
            if (Password.Trim() == "")
            {
                Response.Redirect("Login.aspx", false);
                return "";
            }
            Password = objDAL.Decrypt(Password);
            return Password;
        }
        else
            Response.Redirect("Login.aspx?E=" + Error + "&T=" + IssueType, false);
        return "";
    }

    protected void gvTasks_RowCreated(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                string sortOrder = SortOrder == "DESC" ? "Down" : "Up";
                string sortBy = ViewState["SortExpression"].ToString();

                for (int index = 0; index < gvTasks.Columns.Count; index++)
                {
                    TableCell cell = e.Row.Cells[index];
                    if (!gvTasks.Columns[index].SortExpression.Equals(""))
                    {
                        if (sortBy == gvTasks.Columns[index].SortExpression)
                        {
                            Image imgSorted = new Image();
                            imgSorted.ImageUrl = string.Format("images/BlackArrow{0}.png", sortOrder);
                            cell.Controls.Add(imgSorted);
                        }
                    }
                }
            }

        }
        catch (Exception exception)
        {
            ShowError(exception.Message);
        }
    }

    protected void gvTasks_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortBy = SortExpression;
            string sortOrder = SortOrder;
            SortExpression = e.SortExpression;

            if (e.SortExpression.Equals(sortBy))
                SortOrder = (sortOrder == "ASC" ? "DESC" : "ASC");
            else
                SortOrder = "ASC";
            SortDataGrid();
        }
        catch (Exception exception)
        {
            ShowError(exception.Message);
        }
    }

    protected void gvTasks_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvTasks.PageIndex = e.NewPageIndex;
            gvTasks.DataSource = TasksData;
            gvTasks.DataBind();
        }
        catch (Exception exception)
        {
            ShowError(exception.Message);
        }
    }
    #endregion

    #region Methods
    private DataTable GetTasksData(string UserName, string Password, string DomainName)
    {
        //objErrorLogs.LogError("UserName " +UserName + " Password " + Password + " DomainName " + DomainName);
        ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
        //ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
        service.Credentials = new NetworkCredential(UserName, Password, DomainName);
        //service.Credentials = new NetworkCredential("fahad.khan", "Pakistan1", "innokat");
        service.Url = new Uri(ConfigurationManager.AppSettings["ExchangeServerUrl"]);
        TasksFolder tasksfolder = TasksFolder.Bind(service, WellKnownFolderName.Tasks, new PropertySet(BasePropertySet.IdOnly, FolderSchema.TotalCount));
        if (tasksfolder != null)
        {
            if (tasksfolder.TotalCount > 0)
            {
                ItemView view = new ItemView(tasksfolder.TotalCount);
                view.PropertySet = new PropertySet(BasePropertySet.IdOnly, TaskSchema.Subject, TaskSchema.Owner, TaskSchema.StartDate, TaskSchema.DueDate, TaskSchema.Status, TaskSchema.Importance, TaskSchema.PercentComplete);
                //view.OrderBy = OrderByCollection.
                FindItemsResults<Item> taskItems = service.FindItems(WellKnownFolderName.Tasks, view);
                if (taskItems.TotalCount == 0)
                    return null;
                else
                {
                    DataTable tasksTable = new DataTable();
                    tasksTable.TableName = "Tasks";
                    tasksTable.Columns.Add("Id");
                    tasksTable.Columns.Add("Subject");
                    tasksTable.Columns.Add("AssignedBy");
                    tasksTable.Columns.Add("StartDate");   //, typeof(DateTime)
                    tasksTable.Columns.Add("DueDate");     //, typeof(DateTime)
                    tasksTable.Columns.Add("Status");
                    tasksTable.Columns.Add("Priority");
                    tasksTable.Columns.Add("PercentComplete", typeof(int));
                    ////
                    //tasksTable.Rows.Add(1, "Test 1", "Owner 1", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 2", "Owner 2", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 3", "Owner 3", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 4", "Owner 4", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 5", "Owner 5", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 6", "Owner 6", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 7", "Owner 7", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    //tasksTable.Rows.Add(1, "Test 8", "Owner 8", DateTime.Now, DateTime.Now.AddDays(15), "In Progress", "High", 50);
                    /////
                    foreach (Item item in taskItems)
                    {
                        Task task = item as Task;
                        DataRow taskRow = tasksTable.NewRow();
                        taskRow["Id"] = task.Id;
                        taskRow["Subject"] = task.Subject;
                        taskRow["AssignedBy"] = task.Owner;
                        if (task.StartDate != null)
                            taskRow["StartDate"] = task.StartDate.Value.ToString("dd-MM-yyyy");
                        else
                            taskRow["StartDate"] = "";
                        if (task.DueDate != null)
                            taskRow["DueDate"] = task.DueDate.Value.ToString("dd-MM-yyyy");
                        else
                            taskRow["DueDate"] = "";
                        taskRow["Status"] = task.Status;
                        taskRow["Priority"] = Enum.GetName(typeof(Importance), task.Importance);
                        taskRow["PercentComplete"] = task.PercentComplete;
                        tasksTable.Rows.Add(taskRow);
                    }
                    return tasksTable;
                }
            }
        }
        return null;
    }

    private void SortDataGrid()
    {
        string sortBy = SortExpression;
        string sortOrder = SortOrder;
        DataView tasksView = TasksData.DefaultView;
        if (!string.IsNullOrEmpty(sortBy) && !string.IsNullOrEmpty(sortOrder))
            tasksView.Sort = sortBy + " " + sortOrder;
        gvTasks.DataSource = tasksView;
        gvTasks.DataBind();
    }

    private void ShowError(string message)
    {
        lblError.Text = message;
        lblError.Visible = true;
        gvTasks.Visible = false;
    }
    #endregion
}