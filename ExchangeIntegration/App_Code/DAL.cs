﻿using System;
using System.Collections.Generic;
using System.Web;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using System.Text;
using System.Security.Cryptography;

/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
    public DAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    MySqlConnection objConnection = new MySqlConnection();
    string MySqlConnStr = ConfigurationManager.ConnectionStrings["MySqlConnStr"].ToString();
    string key = "@!n0kat123456789";
    ErrorLogs objErrorLogs = new ErrorLogs();

    private bool ConnectionOpen()
    {
        try
        {
            if (objConnection.State == ConnectionState.Closed)
            {
                objConnection.ConnectionString = MySqlConnStr;
                objConnection.Open();
            }
            return true;
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return false;
        }
    }

    private bool ConnectionClose()
    {
        try
        {
            if (objConnection.State == ConnectionState.Open)
            {
                objConnection.Close();
            }
            return true;
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return false;
        }
    }

    public int ExecuteSQL(string SQL)
    {
        try
        {
            if (ConnectionOpen())
            {

                MySqlCommand objSQLCommand = new MySqlCommand(SQL, objConnection);
                int iResult = objSQLCommand.ExecuteNonQuery();
                ConnectionClose();
                return iResult;
            }
            else
            {
                return -1;
            }
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return -1;
        }
    }

    public DataTable GetDataTable(string SQL)
    {
        try
        {
            if (ConnectionOpen())
            {
                DataTable objDataTable = new DataTable();
                MySqlDataAdapter objDataAdapter = new MySqlDataAdapter(SQL, objConnection);
                objDataAdapter.SelectCommand.CommandTimeout = 700;
                objDataAdapter.Fill(objDataTable);
                ConnectionClose();
                return objDataTable;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return null;
        }
    }

    public string IsUserNameExistinDB(string SQL)
    {
        try
        {
            objErrorLogs.LogError(SQL);
            if (ConnectionOpen())
            {
                objErrorLogs.LogError("Connection Opened");
                DataTable objDataTable = new DataTable();
                MySqlDataAdapter objDataAdapter = new MySqlDataAdapter(SQL, objConnection);
                objDataAdapter.SelectCommand.CommandTimeout = 700;
                objDataAdapter.Fill(objDataTable);
                ConnectionClose();
                if (objDataTable != null)
                {
                    if (objDataTable.Rows.Count > 0)
                        return objDataTable.Rows[0][1].ToString().TrimEnd();
                }
                return "";
            }
            else
            {
                return "";
            }
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return "";
        }
    }

    public string Encrypt(string input)
    {
        try
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return "";
        }
    }

    public string Decrypt(string input)
    {
        try
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes(key);
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return "";
        }
    }

    public string ErrorMessages(string Error)
    {
        try
        {
            switch (Error)
            {
                case "TLAErr":
                    return "The request failed. The remote server returned an error: (401) Unauthorized.";
            }
            return "";
        }
        catch (Exception ex)
        {
            objErrorLogs.LogError(ex);
            return "";
        }
    }
}