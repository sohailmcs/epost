<?php defined('_JEXEC') or die; ?>
<?php
$document = &JFactory::getDocument();
$database = &JFactory::getDBO();

if ($params->get('aweather_layout') == '1') :
$document->addScriptDeclaration("
	window.addEvent('domready', function() {
		var weather = new SideBySlide.Init({
			'wrapper': 'weather_wrpr',
			'slides': '.accord',
			'togglers': '.toggler',
			'defaultView': 0
		});
	});
");
else :
$document->addScriptDeclaration("
	window.addEvent('domready', function() {
		var prayer = new SideBySlide.Init({
			'wrapper': 'prayer_wrpr',
			'slides': '.accord2',
			'togglers': '.toggler2',
			'defaultView': 0
		});
	});
");
endif;

?>
<div class="mod_aweather">
	<?php if ($params->get('aweather_layout') == '1') : ?>
	<div class="warper" id="weather_wrpr">
    	<div class="toggle-holder dubai"><a href="#" class="toggler" rel="dubai"><span>Dubai</span></a></div>
        <div class="accord" id="dubai">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'dubai'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    	<div class="toggle-holder abudhabi"><a href="#" class="toggler" rel="abudhabi"><span>Dubai</span></a></div>
        <div class="accord" id="abudhabi">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'abudhabi'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    	<div class="toggle-holder sharjah"><a href="#" class="toggler" rel="sharjah"><span>Dubai</span></a></div>
        <div class="accord" id="sharjah">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'sharjah'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    	<div class="toggle-holder ajman"><a href="#" class="toggler" rel="ajman"><span>Dubai</span></a></div>
        <div class="accord" id="ajman">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'ajman'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    	<div class="toggle-holder rakhaim"><a href="#" class="toggler" rel="rakhaim"><span>Dubai</span></a></div>
        <div class="accord" id="rakhaim">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'rakhaim'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    	<div class="toggle-holder uaquwain"><a href="#" class="toggler" rel="uaquwain"><span>Dubai</span></a></div>
        <div class="accord" id="uaquwain">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'uaquwain'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    	<div class="toggle-holder fujairah"><a href="#" class="toggler" rel="fujairah"><span>Dubai</span></a></div>
        <div class="accord" id="fujairah">
        	<div class="slide-content">
				<?php
				$database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'fujairah'");
				$item = $database->loadObject();
                ?>
            	<div class="title"><?php print $item->statename; ?></div>
                <div class="weatherinfo">
                	<div class="image" style="background:url(<?php print JURI::base(true) . '/images/weather/' . $item->code; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $item->temp; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $item->high; ?>&deg; | Lo <?php print $item->low; ?>&deg;</div>
                    	<div class="text"><?php print $item->text; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <div class="btmlink"><a href="<?php print $item->w_details; ?>" target="_blank">Complete Week Forecast</a></div>
            </div>
        </div>
    </div>
    <?php else : ?>
	<div class="warper" id="prayer_wrpr">
    	<div class="toggle-holder dubai"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="dubai">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'dubai'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    	<div class="toggle-holder abudhabi"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="abudhabi">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'abudhabi'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    	<div class="toggle-holder sharjah"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="sharjah">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'sharjah'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    	<div class="toggle-holder ajman"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="ajman">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'ajman'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    	<div class="toggle-holder rakhaim"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="rakhaim">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'rakhaim'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    	<div class="toggle-holder uaquwain"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="uaquwain">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'uaquwain'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    	<div class="toggle-holder fujairah"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="fujairah">
        	<div class="slide-content">
            	<div class="prayer_bg">
					<?php
                    $database->setQuery("SELECT * FROM`#__eposts_wnpdata` WHERE `type` = '1' AND `state` = 'fujairah'");
                    $item = $database->loadObject();
                    ?>
                    <div class="title"><span><?php print $item->statename; ?></span><?php print $item->hijri; ?></div>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $item->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $item->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Dhuhr</td>
                        	<td align="right"><?php print $item->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $item->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $item->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $item->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="btmlink"><a href="<?php print $item->p_details; ?>" target="_blank">Complete Month Timing</a></div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>