<?php defined('_JEXEC') or die; ?>
<?php
$document = &JFactory::getDocument();

if ($params->get('aweather_layout') == '1') :
$document->addScriptDeclaration("
	window.addEvent('domready', function() {
		var weather = new SideBySlide.Init({
			'wrapper': 'weather_wrpr',
			'slides': '.accord',
			'togglers': '.toggler',
			'defaultView': 0
		});
	});
");
else :
$document->addScriptDeclaration("
	window.addEvent('domready', function() {
		var prayer = new SideBySlide.Init({
			'wrapper': 'prayer_wrpr',
			'slides': '.accord2',
			'togglers': '.toggler2',
			'defaultView': 0
		});
	});
");
endif;

?>
<div class="mod_aweather">
	<?php if ($params->get('aweather_layout') == '1') : ?>
	<div class="warper" id="weather_wrpr">
    	<?php if ($params->get('dubai') != '') :?>
    	<div class="toggle-holder dubai"><a href="#" class="toggler" rel="dubai"><span>Dubai</span></a></div>
        <div class="accord" id="dubai">
        	<div class="slide-content">
            	<?php if ($params->get('dubaititle') != '') :?>
            	<div class="title"><?php print $params->get('dubaititle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('dubai')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('dubaidtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('dubaidtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('abudhabi') != '') :?>
    	<div class="toggle-holder abudhabi"><a href="#" class="toggler" rel="abudhabi"><span>Dubai</span></a></div>
        <div class="accord" id="abudhabi">
        	<div class="slide-content">
            	<?php if ($params->get('abudhabititle') != '') :?>
            	<div class="title"><?php print $params->get('abudhabititle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('abudhabi')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('abudhabidtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('abudhabidtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('sharjah') != '') :?>
    	<div class="toggle-holder sharjah"><a href="#" class="toggler" rel="sharjah"><span>Dubai</span></a></div>
        <div class="accord" id="sharjah">
        	<div class="slide-content">
            	<?php if ($params->get('sharjahtitle') != '') :?>
            	<div class="title"><?php print $params->get('sharjahtitle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('sharjah')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('sharjahdtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('sharjahdtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('ajman') != '') :?>
    	<div class="toggle-holder ajman"><a href="#" class="toggler" rel="ajman"><span>Dubai</span></a></div>
        <div class="accord" id="ajman">
        	<div class="slide-content">
            	<?php if ($params->get('ajmantitle') != '') :?>
            	<div class="title"><?php print $params->get('ajmantitle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('ajman')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('ajmandtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('ajmandtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('rakhaim') != '') :?>
    	<div class="toggle-holder rakhaim"><a href="#" class="toggler" rel="rakhaim"><span>Dubai</span></a></div>
        <div class="accord" id="rakhaim">
        	<div class="slide-content">
            	<?php if ($params->get('rakhaimtitle') != '') :?>
            	<div class="title"><?php print $params->get('rakhaimtitle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('rakhaim')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('rakhaimdtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('rakhaimdtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('uaquwain') != '') :?>
    	<div class="toggle-holder uaquwain"><a href="#" class="toggler" rel="uaquwain"><span>Dubai</span></a></div>
        <div class="accord" id="uaquwain">
        	<div class="slide-content">
            	<?php if ($params->get('uaquwaintitle') != '') :?>
            	<div class="title"><?php print $params->get('uaquwaintitle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('uaquwain')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('uaquwaindtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('uaquwaindtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('fujairah') != '') :?>
    	<div class="toggle-holder fujairah"><a href="#" class="toggler" rel="fujairah"><span>Dubai</span></a></div>
        <div class="accord" id="fujairah">
        	<div class="slide-content">
            	<?php if ($params->get('fujairahtitle') != '') :?>
            	<div class="title"><?php print $params->get('fujairahtitle'); ?></div>
                <?php endif; ?>
                <div class="weatherinfo">
                	<?php $weather = wheathercast($params->get('fujairah')); ?>
                	<div class="image" style="background:url(http://l.yimg.com/a/i/us/we/52/<?php print $weather->current['code']; ?>.gif) center center no-repeat;"></div>
                	<div class="descp">
                    	<div class="current"><?php print $weather->current['temp']; ?>&deg;C</div>
                    	<div class="status">Hi <?php print $weather->forecast[0]['high']; ?>&deg; | Lo <?php print $weather->forecast[0]['low']; ?>&deg;</div>
                    	<div class="text"><?php print $weather->current['text']; ?></div>
                    </div>
                	<div class="clear"></div>
                </div>
                <?php if ($params->get('fujairahdtl') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('fujairahdtl'); ?>" target="_blank">Complete Week Forecast</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php else : ?>
	<div class="warper" id="prayer_wrpr">
    	<?php if ($params->get('dubai_pr') != '') : ?>
    	<div class="toggle-holder dubai"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="dubai">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('dubai_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('dubaititle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('dubaititle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('dubaidtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('dubaidtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('abudhabi_pr') != '') : ?>
    	<div class="toggle-holder abudhabi"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="abudhabi">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('abudhabi_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('abudhabititle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('abudhabititle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('abudhabidtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('abudhabidtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('sharjah_pr') != '') : ?>
    	<div class="toggle-holder sharjah"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="sharjah">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('sharjah_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('sharjahtitle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('sharjahtitle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('sharjahdtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('sharjahdtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('ajman_pr') != '') : ?>
    	<div class="toggle-holder ajman"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="ajman">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('ajman_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('ajmantitle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('ajmantitle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('ajmandtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('ajmandtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('rakhaim_pr') != '') : ?>
    	<div class="toggle-holder rakhaim"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="rakhaim">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('rakhaim_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('rakhaimtitle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('rakhaimtitle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('rakhaimdtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('rakhaimdtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('uaquwain_pr') != '') : ?>
    	<div class="toggle-holder uaquwain"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="uaquwain">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('uaquwain_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('uaquwaintitle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('uaquwaintitle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('uaquwaindtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('uaquwaindtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    	<?php if ($params->get('fujairah_pr') != '') : ?>
    	<div class="toggle-holder fujairah"><a href="#" class="toggler2" rel="fujairah"><span>01</span></a></div>
        <div class="accord2" id="fujairah">
        	<div class="slide-content">
            	<div class="prayer_bg">
                	<?php
    					$xml = simplexml_load_file($params->get('fujairah_pr'));
						$data = $xml->children();
                    ?>
                    <?php if ($params->get('fujairahtitle_pr') != '') :?>
                    <div class="title"><span><?php print $params->get('fujairahtitle_pr'); ?></span><?php print $data->hijri; ?></div>
                    <?php endif; ?>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    	<tr>
                        	<td>Fajr</td>
                        	<td align="right"><?php print $data->fajr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Ishraq</td>
                        	<td align="right"><?php print $data->sunrise; ?></td>
                        </tr>
                    	<tr>
                        	<td>Zohar</td>
                        	<td align="right"><?php print $data->dhuhr; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Asr</td>
                        	<td align="right"><?php print $data->asr; ?></td>
                        </tr>
                    	<tr>
                        	<td>Magrib</td>
                        	<td align="right"><?php print $data->maghrib; ?></td>
                            <td>&nbsp;&nbsp;</td>
                        	<td>Isha</td>
                        	<td align="right"><?php print $data->isha; ?></td>
                        </tr>
                    </table>
                </div>
                <?php if ($params->get('fujairahdtl_pr') != '') : ?>
                <div class="btmlink"><a href="<?php print $params->get('fujairahdtl_pr'); ?>" target="_blank">Complete Month Timing</a></div>
                <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
</div>
